package org.jakebacker.botgameai;

import com.n9mtq4.botclient.*;
import com.n9mtq4.botclient.world.WorldObject;
import com.n9mtq4.botclient.world.Flag;

public class bot {

    public static void main(String[] args) {


        Game game = new Game();
        int botsAtFlag = 0;
        boolean direction = true;

        //TODO: Make an array list!
        int[] atEnd = new int[20]; //One value per bot, 0 not at end, 1 turned right, 2 turned left


        /*
            0 nothing
            1 block
            2 friendly
            3 last known enemy
         */

        for (int y = 0; y < atEnd.length; y++) {
            atEnd[y] = 0;
        }


        while (true) {
            ControllableBot bot = game.waitForTurn();
            try {
                System.out.println(bot.getUid());
                int i = 0;

                if (game.getTeam() == 1) {
                    for (WorldObject object : bot.getVision()) {
                        if (object.isWall() && object.getY() == bot.getY() + 1) {
                            System.out.println("wall");
                            if (botsAtFlag != 2) {
                                if (atEnd[bot.getUid() - 1] == 0) {
                                    bot.turn(90);
                                    atEnd[bot.getUid() - 1] = 1;
                                    game.endTurn(bot);
                                } else if (atEnd[bot.getUid() - 1] == 1) {
                                    if (object instanceof Flag) {
                                        for (int p = 0; p < 10; p++) {
                                            bot.move(0, 1);
                                        }
                                        botsAtFlag++;
                                        game.endTurn(bot);
                                    }
                                }
                            } else {
                                bot.move(-1, 1);
                                for (int x = 0; x < 9; x++) {
                                    bot.move(0, 1);
                                }
                            }
                        }
                    }
                    for (WorldObject object : bot.getVision()) {
                        if (object.isWall() && object.getY() == bot.getY() + 1) {
                            if (botsAtFlag != 2) {
                                if (atEnd[bot.getUid() - 1] != 2) {
                                    if (object.isWall() && object.getY() == bot.getY() + 1) {
                                        bot.turn(180);

                                        atEnd[bot.getUid() - 1] = 2;
                                        game.endTurn(bot);
                                    }
                                } else {
                                    for (int p = 0; p < 10; p++) {
                                        bot.move(0, 1);
                                    }
                                    botsAtFlag++;
                                    game.endTurn(bot);
                                }
                            } else {
                                bot.move(-1, -1);
                                for (int x = 0; x < 9; x++) {
                                        bot.move(0, 1);
                                }
                            }
                        }
                    }

                    //System.out.println(direction);

                    game.endTurn(bot);
                } else {
                    // System.out.println(direction);
                    for (WorldObject object : bot.getVision()) {
                        if (object.isWall() && object.getY() == bot.getY() - 1) {
                            System.out.println("wall");
                            if (botsAtFlag != 2) {
                                if (atEnd[bot.getUid() - 1] == 0) {
                                    bot.turn(90);
                                    atEnd[bot.getUid() - 1] = 1;
                                    game.endTurn(bot);
                                } else if (atEnd[bot.getUid() - 1] == 1) {
                                    if (object instanceof Flag) {
                                        for (int p = 0; p < 10; p++) {
                                            bot.move(0, 1);
                                        }
                                        botsAtFlag++;
                                        game.endTurn(bot);
                                    }
                                }
                            } else {
                                bot.move(1, 1);
                                for (int x = 0; x < 9; x++) {
                                    bot.move(0, 1);
                                }
                            }
                        }
                    }
                    for (WorldObject object : bot.getVision()) {
                        if (object.isWall() && object.getY() == bot.getY() - 1) {
                            if (botsAtFlag != 2) {
                                if (atEnd[bot.getUid() - 1] != 2) {
                                    if (object.isWall() && object.getY() == bot.getY() - 1) {
                                        try {
                                            bot.turn(180);
                                            atEnd[bot.getUid() - 1] = 2;
                                        }catch (CantPerformActionException e1) {
                                            System.err.println("Not enough action points");
                                        } finally {
                                            game.endTurn(bot);
                                        }
                                    }
                                } else {
                                    for (int p = 0; p < 10; p++) {
                                        bot.move(0, -1);
                                    }
                                    botsAtFlag++;
                                    game.endTurn(bot);
                                }
                            } else {
                                bot.move(-1, 1);
                                for (int x = 0; x < 9; x++) {
                                    bot.move(0, -1);
                                }
                            }
                        }
                    }

                    //System.out.println(direction);
                    game.endTurn(bot);
                }
                game.endTurn(bot);
            } catch (CantPerformActionException e) {
                e.printStackTrace();
                game.endTurn(bot);
            }
        }
    }
}